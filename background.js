// url -> tabId
var openTabs = new Map();
// js Map doesn't have "containsValue" so gotta do a reverse mapping as well.
// tabId -> url
var openURLs = new Map();

const TST_ID = 'treestyletab@piro.sakura.ne.jp';
var hasTST = false
browser.runtime.sendMessage(TST_ID, {
	type: 'ping'
}).then(yes => hasTST = yes)
/*
	rundown:
	1. log opening tab into openTabs array/object/whatever it is
	2. before opening a link, compare it to openTabs
	3. if it exists in openTabs, then:
		-get current tab index
		-move opened link onto index+1 tabs
		-close the tab that was supposed to open
		-modify the openTabs to reflect the new tabId's
	4. if it doesn't exist, goto 1.
*/

/*
 When directing the current tab to a new url,
 onUpdated is called multiple times (twice at least) with 'status: complete' and the same url
 in rapid succession. Don't know why.
 It bugs out because firstly the mapping gets added and then, on the next call, the tab is removed.
 So that's why check if current tab == new url's tab.
 */
function checker(event){
	let url = event.title;
	if (openTabs.has(url)) {
		// Remove asap to avoid flickering tab bar. Except if has TST because it breaks.
		if ( !hasTST ){
			browser.tabs.remove(event.tabId).catch(e => console.error('Open existing: ', e));
		}

		var gettingActiveTab = browser.tabs.get(event.sourceTabId); 
		let activeTabInfo
		gettingActiveTab.then((activeTabInfos) => {			
			activeTabInfo = activeTabInfos

			return browser.tabs.move(openTabs.get(url), {
			// the spot to move the opened tab to
				index: activeTabInfo.index+1,
			})
		}).then(movedTabInfo => {
				
			// TreeStyleTabs
			return browser.runtime.sendMessage(TST_ID, {
				type: 'attach',
				parent: activeTabInfo.id,
				child: movedTabInfo[0].id,
				insertAfter: activeTabInfo.id
			})

			
		})
		.catch((err) => console.error("Open existing: ", err))
		.then(() => browser.tabs.remove(event.tabId))
		.catch(() => {});
	}
}

function addToOpen(event){
	if (event.openerTabId == null) {
		// Basic New Tab that was launched manually (ctrl-t).
		return
	}
	// Not opening a tab but just going into a page. Need to remove the old URL from the lists
	if(openURLs.has(event.id)){
		// If you open a new page you are closing the old one but that doesn't trigger tabs.onRemoved, because it is not a tab closing of course.
		let url = openURLs.get(event.id);
		// Delete the key/value pair that held the "old URL -> old tabId". Next it'll get replaced by "new URL->old tabId"
		openTabs.delete(url); 
	}
	openTabs.set(event.title, event.id);

	//overwrites, np.
	openURLs.set(event.id, event.title);
}

function closedTab(tabId){
	if (openURLs.has(tabId)) {
		let url = openURLs.get(tabId);
		openTabs.delete(url);
		openURLs.delete(tabId);
	}
}

function handleUpdates(id, properties, tab) {
	/*
	Better to match "loading" to look out for redirects.
	Ex. Wikipedia links to https://de.wikipedia.org/wiki/Sozialwissenschaft
	but takes to https://de.wikipedia.org/wiki/Sozialwissenschaften
	Now it doesn't reliably appear as a dupe if you use the latter.
	I'd rather match the first so the *same link* doesn't keep opening new tabs.

	Not sure why properties sometimes appears without url.

	Okay nevermind that. Pages can load directly to complete in SPAs.
	So redirection will have a quirk. Page will load completely before it redirects,
	after that it will be re-checked and handled like any other dupe. But there is a
	delay.
	*/
	if ( !properties.url || properties.status !== 'complete' ) {
		return
	}
	let url = new URL(properties.url)
	// Cut https://, or http://, or..
	url = url.href.slice( url.protocol.length+2 )
	// addToOpen(
	checker(
		{
			id,
			title: url,
			openerTabId: tab.openerTabId
		}
	)
}

browser.tabs.onRemoved.addListener(closedTab);
// onCreated gives an object with the 'title' set to the url, and url set to 'about:blank'.
browser.tabs.onCreated.addListener(checker);
browser.tabs.onUpdated.addListener(handleUpdates, { urls: [ '<all_urls>' ], properties: [ 'status' ] });

